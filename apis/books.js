var express = require('express');
var router = express.Router();
const mongodb = require('mongodb');
var objectId = require('mongodb').ObjectID;

const DB_NAME = 'book_store';
const COLLECTION_NAME = 'Books';

const DB_URI = 'mongodb+srv://dbColin:mongopass@cluster0-o6myu.mongodb.net/admin?retryWrites=true&w=majority';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, {useNewUrlParser: true, useUnifiedTopology: true});

client.connect(function(err, connection) {
  if (err) {
    return res.status(500).send({message: "Could not connect to the database."});
  }

  const db = connection.db(DB_NAME);

  // GET request
  router.get('/', function(req, res) {
    db.collection(COLLECTION_NAME)
      .find({})
        .toArray(function(find_error, result){
          if (find_error)
            return res.status(500).send({message: "Cound not retrieve collection data."});
          return res.send(result);
    });
  });

  // POST request (INSERT Book)
  router.post('/', function(req, res) {
    if (!req.body || req.body.length === 0)
      return res.status(400).send({message: "Object is required."});
    if (!req.body.title || !req.body.author || !req.body.price)
      return res.status(400).send({message: "Required fields: title, author, price"});

    var doc = req.body;

    db.collection(COLLECTION_NAME).insertOne(doc, function(insert_err, result) {
      if (insert_err)
        return res.status(500).send({message: "Could not insert object."});
      return res.send('Object inserted.');
    });
  });

  // PUT request (UPDATE Book)
  router.put('/:id', function(req, res) {
    db.collection(COLLECTION_NAME)
      .updateOne({"_id": objectId(req.params.id)},
        {$set: req.body},
          function(update_err, result) {
      if (update_err)
        return res.status(500).send({message: "Could not update object."});
      return res.send('Object updated.');
    });
  });

  // PUT request (UPDATE Book)
  router.put('/', function(req,res ) {
    db.collection(COLLECTION_NAME)
      .updateOne({"_id": objectId(req.body.id)},
        {$set: req.body},
          function(update_err, result) {
        if (update_err)
          return res.status(500).send({message: "Could not update object."});
        return res.send('Object updated.');
    });
  });

  //DELETE request (DELETE Book)
  router.delete('/:id', function(req, res) {
    db.collection(COLLECTION_NAME)
      .deleteOne({"_id": objectId(req.params.id)},
        function(delete_err, result){
      if (delete_err)
        return res.status(500).send({message: "Could not delete object."});
      return res.send('Object deleted.');
    });
  });

  //DELETE request (DELETE Book)
  router.delete('/', function(req, res) {
    db.collection(COLLECTION_NAME)
      .deleteOne({"_id": objectId(req.body.id)},
        function(delete_err, result){
      if (delete_err)
        return res.status(500).send({message: "Could not delete object."});
      return res.send('Object deleted.');
    });
  });

});

module.exports = router;
