var express = require('express');
var router = express.Router();
const mongodb = require('mongodb');

const DB_NAME = 'book_store';
const USERS_COLLECTION_NAME = 'Users';

const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, {useNewUrlParser: true, useUnifiedTopology: true});

client.connect(function(err, connection) {
  if (err) {
    return res.status(500).send({message: "Could not connect to the database."});
  }

  const db = connection.db(DB_NAME);

  // GET request
  router.get('/', function(req, res) {
    db.collection(USERS_COLLECTION_NAME)
      .find({})
        .toArray(function(find_error, result){
          if (find_error)
            return res.status(500).send({message: "Cound not retrieve collection data."});
          res.send(result);
    });
  });

  // POST request (INSERT User)
  router.post('/', function(req, res) {
    if(!req.body || req.body.lenth === 0)
      return res.status(400).send({message: "Object is required."});
    if(!req.body.name || !req.body.username || !req.body.password)
      return res.status(400).send({message: "Required fields: name, username, password"});

    var doc = req.body;

    db.collection(USERS_COLLECTION_NAME).insertOne(doc, function(insert_err, result) {
      if (insert_err)
        return res.status(500).send({message: "Could not insert object."});
      res.send('Object inserted');
    });
  });

  // PUT request (UPDATE User)
  router.put('/:id', function(req, res) {
    db.collection(USERS_COLLECTION_NAME)
      .updateOne({"_id": objectId(req.params.id)},
        {$set: req.body},
          function(update_err, result) {
      if (update_err)
        return res.status(500).send({message: "Could not update object."});
      res.send('Object updated.');
    });
  });

  //DELETE request (DELETE Book)
  router.delete('/:id', function(req, res) {
    db.collection(USERS_COLLECTION_NAME)
      .deleteOne({"_id": objectId(req.params.id)},
        function(delete_err, result){
      if (delete_err)
        return res.status(500).send({message: "Could not delete object."});
      res.send('Object deleted.');
    });
  });

});

module.exports = router;
